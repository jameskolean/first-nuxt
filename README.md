# My First Nuxt 3 project

This project is my playground to learn Nuxt3. I wanted to deploy a Static version, but it's not supported yet. Instead, I am using [Netlify](https://first-nuxt.netlify.app/).

The most significant bump I hit is the way Layouts work. It does not fit my mental model of how I am used to Layouts working. The lesson here is: Read the manual.

## Create 

```bash
npx nuxi init first-nuxt
cp first-nuxt
pnpm install
pnpm add @pinia/nuxt
pnpm add @nuxtjs/tailwindcss
pnpm install -D eslint prettier eslint-config-prettier eslint-plugin-prettier @nuxtjs/eslint-config-typescript  typescript
pnpm install vue-final-modal
```

Here are the configuration files

```javascript
// nuxt.config.ts
import {
    defineNuxtConfig
} from "nuxt/config"; // replace with `@nuxt/bridge` in v2

export default defineNuxtConfig({
    typescript: {
        shim: false,
    },
    modules: ["@nuxtjs/tailwindcss", "@pinia/nuxt"],
    css: ["@/assets/global.css"],
});
```

```javascript
// plugins/vue-final-modal.js
import {
    defineNuxtPlugin
} from "#app";
import {
    vfmPlugin
} from "vue-final-modal";
export default defineNuxtPlugin((nuxtApp) => {
    if (vfmPlugin) nuxtApp.vueApp.use(vfmPlugin);
});
```

```json
// .eslintrc.js
module.exports = {
  root: true,
  extends: ["@nuxtjs/eslint-config-typescript", "plugin:prettier/recommended"],
  rules: {
    "vue/no-multiple-template-root": "off",
  },
};
```

## Running

```bash
pnpm run dev
```

## Deploy to Netlify

Deploying with Netlify is simply a matter of creating a new Netlify deployment and pointing it at your git repository (GitLab or GitHub or other supported). Netlify will automatically recognize the Nuxt project and set up the build and deployment pipeline.

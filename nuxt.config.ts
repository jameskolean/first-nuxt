// nuxt.config.ts
import { defineNuxtConfig } from "nuxt/config"; // replace with `@nuxt/bridge` in v2

export default defineNuxtConfig({
  typescript: {
    shim: false,
  },
  modules: ["@nuxtjs/tailwindcss", "@pinia/nuxt"],
  css: ["@/assets/global.css"],
});

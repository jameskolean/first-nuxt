// plugins/vue-final-modal.js
import { defineNuxtPlugin } from "#app";
import { vfmPlugin } from "vue-final-modal";
export default defineNuxtPlugin((nuxtApp) => {
  if (vfmPlugin) nuxtApp.vueApp.use(vfmPlugin);
});
